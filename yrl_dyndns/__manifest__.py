# -*- coding: utf-8 -*-
{
    "name": "DynDNS",
    "summary": "Run your own DynDNS service from Odoo, backed by CloudFlare.",
    "description": "Endpoints or agents can dynamically update their dynamic IP, which is replicated to CloudFlare.",
    "author": "YouReal",
    "website": "https://www.youreal.eu",
    "application": True,
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "module_category_operations",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base"],
    # always loaded
    "data": [
        "security/security.xml",
        "security/ir.model.access.csv",
        "views/views.xml",
        "views/templates.xml",
    ],
    # only loaded in demonstration mode
    "demo": ["demo/demo.xml"],
}
