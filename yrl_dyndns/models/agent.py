# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class Agent(models.Model):
    _name = "dyndns.agent"
    _description = "DynDNS Agent"

    name = fields.Char(string=_("Name"), compute="_compute_name_field", store=True)
    owner = fields.Many2one("res.partner", string="Owner")
    description = fields.Char(
        string=_("Description"),
        required=False,
        help=_("e.g. to indicate the specific location of the network."),
    )

    service_profile = fields.Many2one("dyndns.service_profile")

    last_update_ip = fields.Char(
        string=_("Last known IP address"),
        compute="_compute_last_update_fields",
        store=True,
    )
    last_update_on = fields.Datetime(
        string=_("IP address changed"),
        compute="_compute_last_update_fields",
        store=True,
    )
    last_update_ua = fields.Char(
        string=_("Last user agent"), compute="_compute_last_update_fields", store=True
    )
    last_contact = fields.Datetime()

    is_active = fields.Boolean()

    token = fields.Char()

    @api.depends("owner", "description")
    def _compute_name_field(self):
        for agent in self:
            if agent.description:
                agent.name = f"{agent.owner.name} - {agent.description}"
            else:
                agent.name = f"{agent.owner.name}"

    def _compute_last_update_fields(self):
        for agent in self:
            # TODO: look into this
            # For performance issue, when developping a ‘stat button’ (for instance),
            # do not perform a search or a search_count in a loop.
            # It is recommended to use read_group method, to compute all value in only one request.

            # Get the latest update from the agent log
            last_update_ids = self.env["dyndns.agent_log"].search(
                "dyndns.agent_log",
                conditions=[("agent", "=", agent.id)],
                order="updated_on desc",
                limit=1,
            )

            if last_update:
                last_update = self.env["dyndns.agent_log"].read(
                    last_update_ids[0], ["ip_address", "updated_on", "user_agent"]
                )

                agent.last_update_ip = last_update.ip_address
                agent.last_update_on = last_update.updated_on
                agent.last_update_ua = last_update.user_agent
