# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AgentLog(models.Model):
    _name = "dyndns.agent_log"
    _description = "DynDNS Agent Log Entry"

    agent = fields.Many2one("dyndns.agent", string="Agent")
    ip_address = fields.Char()
    updated_on = fields.Datetime()
    user_agent = fields.Char()
