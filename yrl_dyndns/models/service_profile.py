# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ServiceProfile(models.Model):
    _name = "dyndns.service_profile"
    _description = "DynDNS Service Profile"

    name = fields.Char("Name", required=True)
    provider = fields.Selection(selection=[("cloudflare", "Cloudflare")])
    api_token = fields.Char(string="API Token")
